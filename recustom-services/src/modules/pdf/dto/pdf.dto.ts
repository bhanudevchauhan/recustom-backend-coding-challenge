import { IsNotEmpty } from 'class-validator';

export class PdfGenerateInput {
  @IsNotEmpty()
  content: string;

  @IsNotEmpty()
  title: string;

  @IsNotEmpty()
  clip_media: { source: string }[];
}
