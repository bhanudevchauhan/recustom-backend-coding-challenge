import * as request from 'supertest';

import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';

import { AppModule } from '../../../app.module';

import { PdfGenerateInput } from '../dto/pdf.dto';

describe('PDF', () => {
  let app: INestApplication;

  const mockPdfData: PdfGenerateInput = {
    content: '<h1>Hello, World!</h1>',
    title: 'Test PDF',
    clip_media: [{ source: 'test-source' }],
  };

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  it('trigger api successfully', async () => {
    const result = await request(app.getHttpServer())
      .post('/pdf/generate')
      .send(mockPdfData)
      .expect(201);

    expect(result.headers['content-type']).toEqual('application/pdf');
  });
});
