import { Module } from '@nestjs/common';

import { PdfController } from './controllers/pdf.controller';
import { PdfService } from './services/pdf.service';

@Module({
  providers: [PdfService],
  controllers: [PdfController],
})
export class PdfModule {}
