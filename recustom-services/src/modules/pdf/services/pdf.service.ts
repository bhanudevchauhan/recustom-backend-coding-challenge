import { Injectable } from '@nestjs/common';

import { Clip_Media } from '../../../graphql/generated';

import { PdfGenerateInput } from '../dto/pdf.dto';

@Injectable()
export class PdfService {
  convertDataIntoHtml(data: PdfGenerateInput): string {
    let htmlContent = `<h1 style="text-align:center">${data.title}</h1><div>${data.content || ''}</div>`;
    if (data.clip_media && data.clip_media.length > 0) {
      data.clip_media.forEach((image: Clip_Media) => {
        htmlContent +=
          image && image.source
            ? `<img src=${image.source} width="500" />`
            : '';
      });
    }
    return htmlContent;
  }
}
