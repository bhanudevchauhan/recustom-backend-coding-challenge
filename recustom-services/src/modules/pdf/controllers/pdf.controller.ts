import { Response } from 'express';
import * as pdf from 'html-pdf';

import {
  Body,
  Controller,
  HttpException,
  Post,
  Res,
  Logger,
} from '@nestjs/common';

import { PdfService } from '../services/pdf.service';
import { PdfGenerateInput } from '../dto/pdf.dto';
import { OptionsFormate } from '../types/pdf.type';

@Controller('/pdf')
export class PdfController {
  private logger = new Logger(PdfController.name);
  constructor(private pdfService: PdfService) {}

  @Post('/generate')
  async pdfGenerate(
    @Body() data: PdfGenerateInput,
    @Res() res: Response,
  ): Promise<void> {
    try {
      const htmlContent = this.pdfService.convertDataIntoHtml(data);
      const options = {
        format: OptionsFormate.LETTER,
      };
      pdf.create(htmlContent, options).toBuffer((err, resData) => {
        if (err) console.log(err);
        res.setHeader('Content-Type', 'application/pdf');
        res.setHeader(
          'Content-Disposition',
          'attachment; filename=recustom.pdf',
        );
        res.write(resData);
        res.end();
      });
    } catch (error) {
      this.logger.error(error);
      throw new HttpException('Error getting', 500);
    }
  }
}
